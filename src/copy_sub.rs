/// Implemented by heavily modifying tokio_io::copy. Available under a
/// MIT/Apache license here: https://github.com/tokio-rs/tokio-io

use std::io;
use std::io::ErrorKind;

use futures::{Future, Poll, Async};

use tokio_io::{AsyncRead, AsyncWrite};

/// Utility function to shrink slices in a way that satisfies the borrow checker.
fn resize<T>(slice: &mut &mut [T], start: usize, end: usize) {
    use std::mem::swap;
    let mut slice_2 = &mut [] as &mut [T];
    swap(slice, &mut slice_2);
    *slice = &mut slice_2[start .. end];
}

/// Decides whether an ascii char is a word boundary. I decided not to try and
/// use UTF-8 to be somewhat more robust when we are fed binary data.
fn word_boundary(c: u8) -> bool {
    let seperators = [b' ', b'\n', b'\r', b'\t', b'\'', b'"', b'(', b')',
        b',', b'.', b'?', b'{', b'}', b'[', b']',  b'<',
        b'>', b'`', b'!', b':', b';'];

    seperators.contains(&c)
}

/// Copies buf_in to buf_out changing pronouns as in the SUBS list. Replaces
/// buf_out with a slice containing only characters written.
/// Returns the number of characters copied out of buf_in, which may be
/// less than the total if buf_in ends in the possible prefix of a pronoun.
/// (E.g. 'h', or 'he' with no word boundary at the end).
fn sub(buf_in: &[u8], buf_out: &mut &mut [u8]) -> usize {
    const SUBS: [(&[u8], &[u8]); 12] = [
        // Lets promote removing unecessarily gendered language :)
        (b"He", b"They"),
        (b"he", b"they"),
        (b"She", b"They"),
        (b"she", b"they"),
        (b"Her", b"Their"),
        (b"her", b"their"),
        (b"Him", b"Their"),
        (b"him", b"their"),
        (b"Herself", b"Themself"),
        (b"herself", b"themself"),
        (b"Himself", b"Themself"),
        (b"himself", b"themself")
    ];

    let mut out_i = 0;
    let mut word_start = 0;
    for in_i in 0.. buf_in.len() {
        if word_boundary(buf_in[in_i]) {
            let word = &buf_in[word_start .. in_i];
            if let Some(&(_old, new)) = SUBS.iter().find(|&&(old, _new)| old == word) {
                // Copy 'new' into the end of buf_out
                &mut buf_out[out_i .. out_i + new.len()].copy_from_slice(new);
                out_i += new.len();
            }
            else {
                // Copy 'word' into the end of buf_out.
                &mut buf_out[out_i .. out_i + word.len()].copy_from_slice(word);
                out_i += word.len();
            }

            // Copy over the boundary character.
            buf_out[out_i] = buf_in[in_i];
            out_i += 1;

            // Next word starts at the next character. (If the next character
            // is also a word boundary the next word will be empty).
            word_start = in_i + 1;
        }
    }

    resize(buf_out, 0, out_i);
    return word_start;
}

/// A future which will copy all data from a reader into a writer substituting
/// pronouns.
///
/// Created by the [`copy_sub`] function, this future will resolve to the number of
/// bytes copied or an error if one happens.
///
/// [`copy_sub`]: fn.copy_sub.html
#[derive(Debug)]
pub struct CopySub<R, W> {
    reader: Option<R>,
    read_done: bool,
    writer: Option<W>,
    amt: u64,
    in_len: usize,
    out_len: usize,
    in_buf: Box<[u8]>,
    out_buf: Box<[u8]>
}

/// Creates a future which represents copying all the bytes from one object to
/// another substituting pronouns.
///
/// The returned future will copy all the bytes read from `reader` into the
/// `writer` specified. This future will only complete once the `reader` has hit
/// EOF and all bytes have been written to and flushed from the `writer`
/// provided. This reader will wait until reading a word boundary character
/// before copying over data, potentially causing trouble for protocols that
/// don't end with a word boundary.
///
/// On success the number of bytes is returned and the `reader` and `writer` are
/// consumed. On error the error is returned and the I/O objects are consumed as
/// well.
pub fn copy_sub<R, W>(reader: R, writer: W) -> CopySub<R, W>
    where R: AsyncRead,
          W: AsyncWrite,
{
    CopySub {
        reader: Some(reader),
        read_done: false,
        writer: Some(writer),
        amt: 0,
        in_len: 0,
        out_len: 0,
        in_buf: Box::new([0; 1024]),
        out_buf: Box::new([0; 2048])
    }
}

impl<R, W> Future for CopySub<R, W>
    where R: AsyncRead,
          W: AsyncWrite,
{
    type Item = (u64, R, W);
    type Error = io::Error;

    fn poll(&mut self) -> Poll<(u64, R, W), io::Error> {
        let mut blocked = false;
        while !blocked {
            // We set blocked here, and then we unset it if we
            // can succesfully read or write without blocking.
            blocked = true;

            // If our buffer isn't full, try and read in some data.
            if self.in_len != self.in_buf.len() && !self.read_done {
                let reader = self.reader.as_mut().unwrap();

                match reader.read(&mut self.in_buf[self.in_len ..]) {
                    Ok(n) => {
                        if n == 0 {
                            self.read_done = true;
                        } else {
                            self.in_len += n;

                            // Copy text to outgoing buf doing substitutions.
                            let mut out_buf_local = &mut self.out_buf[self.out_len ..];
                            let copied = sub(&self.in_buf[0.. self.in_len],
                                             &mut out_buf_local);
                            self.out_len += out_buf_local.len();

                            // Move remaining characters to front of buf
                            self.in_len -= copied;
                            for i in 0.. self.in_len {
                                self.in_buf[i] = self.in_buf[i + copied];
                            }
                        }

                        blocked = false;
                    },
                    // Nothing needs to happen in this case. We will report we
                    // are blocked unless the write doesn't block.
                    Err(ref e) if e.kind() == ErrorKind::WouldBlock => {}
                    Err(e) => return Err(e.into()),
                }
            }

            // If our buffer has some data, let's write it out!
            while self.out_len != 0 {
                let writer = self.writer.as_mut().unwrap();
                match writer.write(&self.out_buf[0 .. self.out_len]) {
                    Ok(n) => {
                        self.amt += n as u64;

                        // Move remaining characters to front of buf.
                        self.out_len -= n;
                        for i in 0.. self.out_len {
                            self.out_buf[i] = self.out_buf[i + n];
                        }

                        blocked = false;
                    }
                    // Nothing happens here in the blocking case. Unless the read
                    // side isn't blocking we will return NotReady.
                    Err(ref e) if e.kind() == ErrorKind::WouldBlock => {}
                    Err(e) => return Err(e.into()),
                }
            }

            // If we've written al the data and we've seen EOF, flush out the
            // data and finish the transfer.
            // done with the entire transfer.
            if self.out_len == 0 && self.read_done {
                try_nb!(self.writer.as_mut().unwrap().flush());
                let reader = self.reader.take().unwrap();
                let writer = self.writer.take().unwrap();
                return Ok((self.amt, reader, writer).into())
            }
        }

        Ok(Async::NotReady)
    }
}
