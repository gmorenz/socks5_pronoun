#![feature(conservative_impl_trait)]
#![allow(dead_code)]

extern crate futures;
extern crate tokio_core;
#[macro_use]
extern crate tokio_io;

mod copy_sub;

use std::io;
use std::net::{IpAddr, SocketAddr};

use futures::{Future, Stream};
use futures::future::{err, ok};
use tokio_core::net::{TcpListener, TcpStream};
use tokio_core::reactor::Core;
use tokio_io::{AsyncRead, AsyncWrite};
use tokio_io::io::{read_exact, write_all, copy};

use copy_sub::copy_sub;

enum ConnectionError {
    ServerFailure = 1,
    Forbidden = 2,
    NetworkUnreachable = 3,
    HostUnreachable = 4,
    ConnectionRefused = 5,
    TTLExpired = 6,
    CommandNotSupported = 7,
    AddressTypeNotSupport = 8
}

enum Error {
    Io(io::Error),
    Connection(TcpStream, ConnectionError)
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::Io(e)
    }
}

impl From<(TcpStream, ConnectionError)> for Error {
    fn from(e: (TcpStream, ConnectionError)) -> Error {
        Error::Connection(e.0, e.1)
    }
}

// type IoFuture<T> = Future<Item = T, Error = io::Error>
// like IoFuture in tokio_io except without the box.
trait IoFuture<T>: Future<Item = T, Error = io::Error> {}
impl<T, F: Future<Item = T, Error = io::Error>> IoFuture<T> for F {}

// type MyFuture<T> = Future<Item = T, Error = Error>
trait MyFuture<T>: Future<Item = T, Error = Error> {}
impl<T, F: Future<Item = T, Error = Error>> MyFuture<T> for F {}

fn read_byte<R: AsyncRead>(input: R) -> impl IoFuture<(R, u8)> {
    read_exact(input, [0; 1]).map(|(input, buf)| (input, buf[0]))
}

// Explicit lifetime because Impl trait was insisting on it *shrug*.
fn parse_addr<'a> (input: TcpStream) -> impl MyFuture<(TcpStream, IpAddr)> + 'a {
    read_byte(input)
        .map_err(From::from)
        .and_then(|(input, atype)| {
            use IpAddr::*;
            use std::net::{Ipv4Addr, Ipv6Addr};

            // Explicit type needed for force conversion.
            let res: Box<Future<Item = (TcpStream, IpAddr), Error = Error>> = match atype {
                1 => Box::new(read_exact(input, [0u8; 4])
                    .map(|(input, buf)| (input, V4(Ipv4Addr::from(buf))))
                    .map_err(From::from)),
                // TODO: DNS lookup here?
                3 => {
                    println!("Trying to run dns lookup");
                    Box::new(err((input, ConnectionError::AddressTypeNotSupport).into()))
                },
                4 => Box::new(read_exact(input, [0u8; 16])
                    .map(|(input, buf)| (input, V6(Ipv6Addr::from(buf))))
                    .map_err(From::from)),
                _ => {
                    println!("Unknown address type");
                    Box::new(err((input, ConnectionError::AddressTypeNotSupport).into()))
                }
            };

            res
        })
}

struct Request {
    version: u8,
    command: u8,
    address: SocketAddr
}

impl Request {
    fn parse<'a>(input: TcpStream) -> impl MyFuture<(TcpStream, Request)> + 'a {
        read_exact(input, [0u8; 3])
            .map_err(From::from)
            // TODO: Confirm buf[2] == 0 or error
            // Parse Addresses
            .and_then(|(input, buf)|
                parse_addr(input).map(move |(input, address)| {
                    (input, buf[0], buf[1], address)
                })
            )
            // Parse port and build request
            .and_then(|(input, version, command, address)|
                read_exact(input, [0u8; 2])
                    .map_err(From::from)
                    .map(move |(input, portbuf)| {
                        let port = ((portbuf[0] as u16) << 8) + portbuf[1] as u16;

                        (input,
                            Request {
                                version,
                                command,
                                address: SocketAddr::new(address, port)
                            }
                        )
                    })
            )
    }
}

// TODO: Is there a standard name for this?
struct MethodsOffer {
    version: u8,
    methods: Vec<u8>,
}

impl MethodsOffer {
    fn parse<R: AsyncRead>(input: R) -> impl IoFuture<(R, MethodsOffer)> {
        read_exact(input, [0u8; 2]).and_then(|(input, buf)| {
            read_exact(input, vec![0u8; buf[1] as usize]).map(move |(input, methods)| {
                (
                    input,
                    MethodsOffer {
                        version: buf[0],
                        methods: methods,
                    },
                )
            })
        })
    }
}

struct Reply {
    reply: u8,
    // Technically I could also reply with a domain name, but that seems
    // ... unnecessary.
    address: SocketAddr,
}

impl From<ConnectionError> for Reply {
    fn from(err: ConnectionError) -> Reply {
        use ConnectionError::*;
        let reply = match err {
            ServerFailure => 1,
            Forbidden => 2,
            NetworkUnreachable => 3,
            HostUnreachable => 4,
            ConnectionRefused => 5,
            TTLExpired => 6,
            CommandNotSupported => 7,
            AddressTypeNotSupport => 8
        };

        Reply {
            reply,
            address: SocketAddr::from(([0, 0, 0, 0], 0))
        }
    }
}

impl Reply {
    fn send<W: AsyncWrite>(self, out: W) -> impl IoFuture<W> {
        let mut buf = vec![5, self.reply, 0];

        match self.address {
            SocketAddr::V4(addr) => {
                buf.push(1);
                buf.extend(&addr.ip().octets());
            },
            SocketAddr::V6(addr) => {
                buf.push(4);
                buf.extend(&addr.ip().octets());
            }
        }

        let port = self.address.port();
        buf.extend(&[(port & 0xff00 >> 8) as u8, (port & 0xff) as u8]);

        write_all(out, buf).map(|(w, _buf)| w)
    }
}

fn main() {
    // Create the event loop that will drive this server
    let mut core = Core::new().unwrap();
    let handle = core.handle();

    // Bind the server's socket
    let addr = "127.0.0.1:12345".parse().unwrap();
    let listener = TcpListener::bind(&addr, &handle).unwrap();

    let server = listener.incoming().for_each(|(client_stream, _)| {
        let handle2 = handle.clone();

        // Recieve authentication methods from the client, and answer
        // with no authentication (or an error).
        let offer = MethodsOffer::parse(client_stream);
        let no_auth = offer.and_then(|(client_stream, offer)| {
            if offer.version != 5 || !offer.methods.contains(&0) {
                println!("Wrong version or no supported method");

                write_all(client_stream, [5, 0xff])
                // Let the client close the connection as in the spec
                // (And it would be somewhat inconvenient to do here.)
            }
            else {
                write_all(client_stream, [5, 0])
            }
        });

        // Get the connection request from the client
        let request = no_auth
            .map_err(From::from)
            .and_then(|(client_stream, _buf)| Request::parse(client_stream));

        // Set up a TCP stream and report back (or error if they asked for
        // something else).
        let connect = request
            .and_then(move |(client_stream, request)| {
                println!("Connecting");

                // Only support connect right now
                if request.command != 1 {
                    println!("Dropping, unsuported command");
                    return Box::new(err((client_stream, ConnectionError::CommandNotSupported).into()))
                        as Box<Future<Item = (TcpStream, TcpStream), Error=Error>>
                };

                let res = TcpStream::connect(&request.address, &handle2)
                    // TODO: Translate these errors into ConnectionErrors to send
                    // to the client.
                    .then(|res| {
                        // Conceptually this is just a `.map` and a `.map_err`,
                        // but both need to pass along the client server_stream so we
                        // do it like this to satisfy the borrow checker.
                        // Ideally I would use a `.map_both` function, but that
                        // doesn't exist.

                        match res {
                            Ok(server_stream) => ok((client_stream, server_stream)),
                            Err(error) => {
                                use ConnectionError::*;

                                // std doesn't provide another way to get enough
                                // detail (well, apart from maybe matching on errors
                                // descriptions).
                                let error = match error.raw_os_error() {
                                    Some(101 /* ENETUNREACH */ ) => NetworkUnreachable,
                                    Some(111 /* ECONNREFUSED */ ) => ConnectionRefused,
                                    Some(113 /* EHOSTUNREACH */ ) => HostUnreachable,
                                    _ => ServerFailure
                                };
                                err(Error::Connection(client_stream, error))
                            }
                        }
                    });

                Box::new(res)
            });

        // Start proxying the two tcp streams (clients, and the one you
        // just set up to some server).
        let reply_and_proxy = connect.then(|maybe_streams| {
            match maybe_streams {
                // Don't handle io errors with the client
                Err(Error::Io(error)) => {
                    Box::new(err(error))
                    as Box<Future<Item = (), Error = io::Error>>
                },
                // Tell the client if we failed to connect through.
                // Then drop it.
                Err(Error::Connection(client_stream, x)) => {
                    println!("Proxy error");
                    Box::new(Reply::from(x).send(client_stream).map(|_| ()))
                },
                Ok((client_stream, server_stream)) => {
                    println!("Final send sequence");
                    Box::new(Reply {
                        reply: 0,
                        address: server_stream.local_addr().unwrap()
                    }.send(client_stream)
                        .and_then(|client_stream| {
                            println!("Setting up copy");

                            let (read_1, write_1) = client_stream.split();
                            let (read_2, write_2) = server_stream.split();
                            copy(read_1, write_2).join(
                                copy_sub(read_2, write_1)
                            )
                        })
                        .map(|_| ()))
                }
            }
        });

        let discard_io_errors = reply_and_proxy.map_err(|err| println!("IO Error: {:?}", err));

        handle.spawn(discard_io_errors);

        Ok(())
    });

    core.run(server).expect("core.run(server) failed");
}
